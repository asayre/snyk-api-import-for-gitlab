
# Snyk API Import tool for GitLab and MacOS

A how-to guide to 1) set up Snyk's [API Import tool](https://github.com/snyk-tech-services/snyk-api-import) for an on-prem GitLab environment, 2) create Snyk orgs for all your GitLab groups, and 3) import all your GitLab repos into each Snyk org

...and learn a little Linux command-line along the way

> These instructions are for MacOS, but can be adapted to most Linux OS distros by doing things like changing the `.zshrc` file references to `.bashrc`.
If you are doing this on Windows, I got nothing...

---

## Gather values

- A GitLab access token with proper permissions: https://docs.snyk.io/integrations/git-repository-scm-integrations/gitlab-integration

- A Snyk Svc Acct Token: https://docs.snyk.io/user-and-group-management/structure-account-for-high-application-performance/service-accounts#how-to-set-up-a-group-or-organization-service-account

- A Snyk Group ID:

    ![image](Group-ID.png)

- A Snyk 'template' Org ID

    > The ID of a Snyk org with settings you want to use for all the new Snyk orgs you'll create a bit later

    ![image](Org-ID.png)

---

## Set up

Open your terminal

Ensure you are in your home directory

```bash
cd ~
```
Ensure npm is installed

```bash
brew install npm
```

Install the Snyk API Import tool:

```bash
npm install snyk-api-import@latest -g
```
Install jq:

```bash
brew install jq
```

## Create persistent variables

Ensure you have a run time configuration file:

```bash
ls ~/.zshrc
```

If you get an error message like...

```
... No such file or directory
```
    
...then you need to create the file:

```bash
touch ~/.zshrc
```
> If no error message, skip the `touch...` command and go to the next step:

Open the file in `textedit`...

```bash
open -a textedit ~/.zshrc
```

... and paste these commands in it:

```bash
export SNYK_TOKEN=(paste snyk token here)
export GITLAB_TOKEN=(paste GitLab token here)
export SNYK_GROUP_ID=(paste Snyk group ID here)
export SOURCE_ORG_PUBLIC_ID=(paste Snyk template org Id here)
export SOURCE_URL=(paste url to local GitLab server here, including 'https://')
export SNYK_LOG_PATH=~/gitlab-api-import/snyk-log
```

Save and close file

Reload the file:

```bash
source ~/.zshrc
```

Verify the vars are captured:

```bash
echo $SNYK_TOKEN
echo $GITLAB_TOKEN
echo $SNYK_GROUP_ID
echo $SOURCE_ORG_PUBLIC_ID
echo $SOURCE_URL
echo $SNYK_LOG_PATH
```

If you see the values you pasted in, you're done with this step

[Note]: Capturing variables this way makes them persistent through terminal sessions and reboots

---

## Make directories

Ensure you are in your home directory:

```bash
cd ~
```

Make a parent directory:

```bash
mkdir gitlab_snyk_import
```
Navigate into the directory you just created:
```bash
cd gitlab_snyk_import
```

Make a subdirectory inside it:
```bash
mkdir snyk-log
```
---
## Import steps

**Create Group file:**

Ensure you are in the `gitlab-snyk-import` directory:

```bash
cd ~/gitlab-snyk-import
```

> REMEMBER: All bash commands from this point forward need to be executed from this directory: ~/gitlab-snyk-import

> If a command fails with an error message like: "...no such file or directory", check what directory you are in with this command: `pwd`, repeat the above `cd` command if you are not in the correct directory

The next command will create a list of all the GitLab groups and subgroups accessible by the GitLab token... 

```bash
DEBUG=snyk* snyk-api-import orgs:data --groupId=$SNYK_GROUP_ID --source=gitlab --sourceUrl=$SOURCE_URL --sourceOrgPublicId=$SOURCE_ORG_PUBLIC_ID
```

...and save it to a json file in the subdirectory `snyk-log` and name it like: `group-$SNYK_GROUP_ID-gitlab-orgs.json`

Confirm the file was created:
```bash
ls snyk-log/
```


Review the file:
```bash
jq . snyk-log/group-$SNYK_GROUP_ID-gitlab-orgs.json
```

Example:
![image](Group-file.png)

---

**Create new Snyk Orgs:**

This command will use the Group file to create new Snyk orgs in your Snyk tenant, with settings that match the template org you specified:

```bash
DEBUG=snyk* snyk-api-import orgs:create --noDuplicateNames --file=snyk-log/group-$SNYK_GROUP_ID-gitlab-orgs.json
```

> A file called `snyk-created-logs.json` will be created in the snyk-log directory

Review the file
```bash
jq . snyk-log/snyk-created-orgs.json 
```

---

**Create Project import file:**

Use the results of the file from the previous step... to create a new file listing out all projects to import between each **GitLab Group:Snyk Org** combo

```bash
DEBUG=snyk* snyk-api-import import:data --orgsData=snyk-log/snyk-created-orgs.json --source=gitlab --sourceUrl=$SOURCE_URL --sourceOrgPublicId=$SOURCE_ORG_PUBLIC_ID
```
> A file called `gitlab-import-targets.json` will be created in the snyk-log directory

Review the file:
```bash
jq . snyk-log/gitlab-import-targets.json
```

> You can manually edit the file as needed or leave as is for the next step 

---

**Import files:**

Use the `gitlab-import-targets.json` file to execute the import

```bash
snyk-api-import import --file=snyk-log/gitlab-import-targets.json
```


Other documentation: 

https://docs.snyk.io/other-tools/tool-snyk-api-import